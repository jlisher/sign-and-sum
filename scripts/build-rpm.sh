#!/usr/bin/env bash
set -euo pipefail
IFS=$'\t\n'

function _build() {
  local -r base_dir="$(dirname "$(realpath "${0%/*}")")"

  local -r package_name="$(cat "${base_dir}/.name")"
  local -r package_version="$(cat "${base_dir}/.version")"
  local -r package_build="$(cat "${base_dir}/.build")"
  local -r package_tar="${package_name}-${package_version}.tar.gz"
  local -r package_spec_file="${base_dir}/${package_name}.spec"

  local -r source_remote="sources.jlisher.com"
  local -r gpg_base_url="https://${source_remote}/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg"
  local -r source_base_url="https://${source_remote}/${package_name}/${package_tar}"
  local -r source_dir="${HOME}/rpmbuild/SOURCES"
  local -r spec_dir="${HOME}/rpmbuild/SPECS"
  local -r spec_file="${spec_dir}/${package_name}.spec"

  pushd "${source_dir}" 1>/dev/null

  curl --remote-time \
       --remote-name-all \
       --location \
       --ssl-reqd \
       --tlsv1.2 \
       "${source_base_url}{,.sha256sum}{,.asc}" \
       "${gpg_base_url}{,.sha256sum}"

  popd 1>/dev/null

  cp -f -t "${spec_dir}" "${package_spec_file}"

  rpmbuild -bs "${spec_file}"

  local -r srpm_dir="${HOME}/rpmbuild/SRPMS"
  local -r srpm="${package_name}-${package_version}-${package_build}.src.rpm"
  local -r result_base="${base_dir}/builds"
  local -ar build_roots=("fedora-35-x86_64" "fedora-36-x86_64" "centos-stream-8-x86_64" "centos-stream-9-x86_64")

  ! [[ -d "${result_base}" ]] || mkdir -p "${result_base}"

  for cfg in "${build_roots[@]}"; do
    result_dir="${result_base}/${cfg//-/\/}"

    ! [[ -d "${result_dir}" ]] || rm -rf "${result_dir}"

    mkdir -p "${result_dir}"

    mock --root "${cfg}" --resultdir "${result_dir}" --rebuild "${srpm_dir}/${srpm}"
  done
}

_build
