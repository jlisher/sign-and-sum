Name:           sign-and-sum
Version:        0.1.1
Release:        1%{?dist}
Summary:        Generates sha256sums and GPG signatures
BuildArch:      noarch

License:        GPLv3+
URL:            https://gitlab.com/jlisher/%{name}/

Source0:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz
Source1:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.asc
Source2:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum
Source3:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum.asc
Source4:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg
Source5:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg.sha256sum

BuildRequires:  gnupg2
Requires:       (coreutils or coreutils-single)
Requires:       gnupg2
Requires:       rpm-sign

%description
Provides a command used for generating sha256sums and GPG signatures for the
jlisher packaging repos and sources.

%prep
# make sure everything runs successfully
set -euo pipefail

# move into the source directory to verify files
pushd %{_sourcedir}

# Verify GPG keyring checksum (consistency)
sha256sum -c %{SOURCE5}

# Verify tarball checksum GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE3}' --data='%{SOURCE2}'

# Verify tarball checksum (consistency)
sha256sum -c %{SOURCE2}

# Verify tarball GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE1}' --data='%{SOURCE0}'

# leave the source directory
popd

# Run setup macro
%setup -q

%build

%install
install -Z -m 755 -d %{buildroot}/%{_bindir}
install -Z -m 755 -t %{buildroot}/%{_bindir} bin/sign-and-sum

%files
%{_bindir}/sign-and-sum
%license LICENSE
%doc README.md

%changelog
* Mon Apr 25 2022 Jarryd Lisher <jarryd@jlisher.com> 0.1.1-1
- Update checksum validation
- Update directory changing function
* Fri Apr 22 2022 Jarryd Lisher <jarryd@jlisher.com> 0.1.0-1
- Update build and publishing scripts
- Support all file types
* Fri Apr 15 2022 Jarryd Lisher <jarryd@jlisher.com> 0.0.2-2
- Update dependencies
* Wed Apr 13 2022 Jarryd Lisher <jarryd@jlisher.com> 0.0.2-1
- Some minor updates
* Fri Apr 08 2022 Jarryd Lisher <jarryd@jlisher.com> 0.0.1-1
- Initial release
