# Sign And Sum

This is a simple package that provides the `sign-and-sum` command used for publishing packages in the jlisher packaging
[repos](https://rpms.jlisher.com/) and [sources](https://sources.jlisher.com/).

## Usage

`sign-and-sum` accepts a list of files. Each file will be signed using the GPG key
stored in the `GPG_SIGNING_KEY` environment variable and have a sha256sum generated.

```shell
sign-and-sum FILE [FILE...]
```

## Environment Variables

| Name            | Default                         | Description         |
| ---             | ---                             | ---                 |
| GNUPGHOME       | /srv/.gpg                       | GPG home directory  |
| GPG_SIGNING_KEY | /srv/.gpg/jlisher-packaging.key | GPG signing key     |
